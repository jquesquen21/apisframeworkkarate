package ApisTiendaMascota;

import com.intuit.karate.junit5.Karate;


public class KarateRunner {

    @Karate.Test
    Karate testSample() {
        return Karate.run("classpath:ApisTiendaMascota/Get_Listar.feature")
                .karateEnv("cert")
                .tags("@GetListar");
    }
}
