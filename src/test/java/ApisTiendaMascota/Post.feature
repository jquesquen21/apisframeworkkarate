Feature: Tienda de mascota APIS

  Background:
    Given url swaggerUrl
    * def bodyUser = read('classpath:Data/bodyUser.json')

  @postUser
  Scenario Outline: Crear usuario
    Given path '/user'
    * configure headers = { accept: 'application/json', Content-Type: 'application/json'}
    * set bodyUser.id = <id>
    * set bodyUser.username = '<username>'
    * set bodyUser.firstName = '<firstName>'
    * set bodyUser.lastName = '<lastName>'
    * set bodyUser.email = '<email>'
    * set bodyUser.password = '<password>'
    * set bodyUser.phone = '<phone>'
    * set bodyUser.userStatus = '<userStatus>'
    And request bodyUser
    When method post
    Then status 200
    * print response

    Examples:
      | id | username | firstName | lastName | email                | password | phone     | userStatus |
      | 5  | user1    | Juan      | Cordova  | cordova21@gmail.com  | Peru0001 | 987456123 | 1          |
      | 6  | user2    | Alfredo   | Quesquen | quesquen21@gmail.com | Peru0002 | 987456123 | 1          |

    @GetUser
    Scenario Outline: recuperar datos del usuario creado
      Given path '/user/' + '<username>'
      * configure headers = { accept: 'application/json'}
      When method get
      Then status 200
      * print response

      Examples:
        | username |
        | user1    |
        | user2    |




