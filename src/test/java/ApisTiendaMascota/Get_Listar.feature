Feature: Tienda de mascota APIS

  Background:
    Given url swaggerUrl
    * configure headers = {'Content-Type': 'application/json'}

  @GetListar
  Scenario Outline: Obtener nombres de mascotas vendidas y contar cuantas se llaman igual
    Given path 'pet/findByStatus'
    And param status = '<status>'
    When method get
    Then status 200
    * def pets = response
    And print pets
    * def soldPets = karate.filter(pets, pet => pet.status == '<status>')
    And print soldPets
    * def petNames = karate.map(soldPets, pet => ({id: pet.id, name: pet.name}))
    * def petJson = karate.toJson(petNames)
    * karate.log('JSON generado:', petJson)
    * def fileName = 'src/test/java/data/soldPets.json'
    And print fileName
    * def filePath = java.nio.file.Paths.get(fileName)
    And print filePath
    * karate.write(petJson,fileName)
    And def PetCounter =
       """
          function(pets) {
          var names = {}
          for(var i = 0; i < pets.length; i++) {
          var name = pets[i].name
          if(names[name]) {
            names[name] += 1
          } else {
            names[name] = 1
          }
          }
          return names
        }
      """
    * def petCount = PetCounter(petNames)
    * print 'Pet count by name:', petCount

    Examples:
    |status|
    |sold  |

