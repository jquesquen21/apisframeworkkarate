function fn() { karate.configure('ssl', true);
    var env = karate.env;
    var swaggerUrl = '';
    karate.log('Se realizó la ejecución en el ambiente: ', env);

 if (!env) {
     env = 'dev';
 }
 if (env === 'dev') {

 } else if (env === 'cert') {
    swaggerUrl = 'https://petstore.swagger.io/v2'
 }

 var config = {
    env: env,
    swaggerUrl: swaggerUrl,
    soldPetDirectory: 'src/test/java/data'
 }

 return config;
 }